import React from 'react';


import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import { blue } from '@mui/material/colors';

  const AluminiButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(blue[900]),
    backgroundColor: blue[900],
    borderRadius: 10,
    textTransform: 'none',
    color: 'white',
    '&:hover': {
      backgroundColor: blue[700],
    },
  }));

  



const BodyLeft = () => {
    return(
      <div className="body_left" style={{width: "350px",padding: "110px",boxSizing: 'border-box'}}>
      <AluminiButton variant="contained">CREATE DRIVES</AluminiButton>
      <AluminiButton variant="contained">VIEW STUDENT LIST</AluminiButton>
      <AluminiButton variant="contained">VIEW DRIVERS</AluminiButton>
        </div>
        
    )
}


export default BodyLeft;